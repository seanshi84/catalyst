<?php

function checkFile($options)
{
    if (!isset($options['file'])) {
        //error_log('this is a test error msg');
        throw new Exception('Missing CSV file. Type --help for the instruction.');
    }
    if (!file_exists($options['file'])) {
        throw new Exception('Can\'t find the import file.');
    }
    if (!is_readable($options['file'])) {
        throw new Exception('Import file is not readable');
    }

    return $options['file'];
}

function insertRecords($filename, $db, $delimiter = ",")
{
    $file = fopen($filename, "r");
    $existing_emails = array();  // store existing emails in the table to check for duplicates
    $row = 0;
    $fail_row = 0;

    if ($db) {
        if ($db->check_user_table()) {
            print('Users table is not empty' . "\r\n");
        }
    }

    while (($csv = fgetcsv($file, 10000, $delimiter)) !== FALSE) {
        $row++;
        if ($row == 1) {
            continue;  //skip the first row
        }

        $name = pg_escape_string(user_name($csv[0])); // use pg_escape_string for apostrophe
        $surname = pg_escape_string(user_name($csv[1]));
        $email = pg_escape_string(user_email($csv[2]));

        if (in_array($email, $existing_emails)) {
            print('Duplicate emails at row: ' . $row . "\r\n");
            error_reporting('Duplicate emails at row: ' . $row . "\r\n");
            $fail_row++;
            continue;  //skip duplicate email rows
        }

        $existing_emails[] = $email;

        if (!$email) {
            print('Invalid email at row: ' . $row . "\r\n");
            error_reporting('Invalid email at row: ' . $row . "\r\n");
            $fail_row++;
            continue;
        }

        if ($db) {
            $result = $db->insertRecords($name, $surname, $email);
            if (!$result) {
                print('Insert query failed at row: ' . $row . "\r\n");
                error_reporting('Insert query failed at row: ' . $row . "\r\n");
                $fail_row++;
            }
        }
    }
    $total = $row - 1; //skip the first row
    $success = $total - $fail_row;
    if (!$db) {
        print("This is a dry run mode. Only for check existing records. No database changes." . "\r\n");
    }
    print("Total rows to import: $total, failed rows: $fail_row. Success rows: $success" . "\r\n");
}

/**
 * Check if name is valid
 * @param $name
 * @return string
 */
function user_name($name)
{
    return ucwords(trim($name));
}

/**
 * Check if Email is valid
 * @param $email
 * @return string
 */
function user_email($email)
{
    $email = trim(strtolower($email));
    if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email)) {
        return ''; //invalid email
    }
    return $email;
}


function helpInfo()
{
    print("
    --file [csv file name]   this is the name of the CSV to be parsed
    --create_table   this will cause the PostgreSQL users table to be built (and no further action will be taken)
    --dry_run   this will be used with the --file directive in the instance that we want to run the script but not insert into the DB. All other functions will be executed, but the database won't be altered.
    -u    PostgreSQL username
    -p    PostgreSQL password
    -h    PostgreSQL host. Default is 'localhost'
    -db   PostgreSQL database name. Default is PostgreSQL username
    --help  which will output the above list of directives with details. (and no further action will be taken)    
    ");
}
