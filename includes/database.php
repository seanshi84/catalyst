<?php

/**
 * This class handle all the db actions.
 */
class database
{

    public $db;
    private $user;
    private $pwd;
    private $host = 'localhost';
    private $dbname;

    /**
     * Construct for db
     * @params string $user, $pwd, $host, $dbname
     *
     * @return void
     *
     */
    public function __construct($user, $pwd, $host, $dbname)
    {
        $this->user = $user;
        $this->pwd = $pwd;

        $this->host = $host;
        $this->dbname = $dbname;

        $this->db = $this->db_connection();
    }

    private function checkdb()
    {
        if (!$this->db) {
            die('db connection failed');
        }
    }

    public function db_connection()
    {
        $db = pg_connect("host=$this->host dbname=$this->dbname 
            user=$this->user password=$this->pwd") or die('db connection failed');
        return $db;
    }

    /**
     *  @return void
     */
    public function create_user_table()
    {
        $this->checkdb();
        //Check if users table exists then drop first
        $sql = 'DROP TABLE IF EXISTS users';
        pg_exec($this->db, $sql) or die(pg_errormessage());

        //Create new users table
        $sql = 'CREATE TABLE users (
            name varchar(40),
            surname varchar(40),
            email CHARACTER VARYING(255) NOT NULL UNIQUE
        )';
        pg_exec($this->db, $sql) or die(pg_errormessage());
    }

    /**
     * Check if user table is empty
     * @return int
     */
    public function check_user_table(){
        $this->checkdb();
        $sql = 'SELECT * FROM users';
        $result = pg_query($this->db, $sql);
        return pg_num_rows($result);
    }

    /**
     * @param $name
     * @param $surname
     * @param $email
     * @return resource
     */
    public function insertRecords($name, $surname, $email)
    {
        $this->checkdb();
        $sql = "INSERT INTO users VALUES ('" . $name . "','" . $surname . "', '" . $email . "')";
        $result = pg_query($this->db, $sql);

        return $result;
    }

}