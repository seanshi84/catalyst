<?php

// Comment these lines to hide errors
error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'includes/database.php';
require 'includes/functions.php';

function init(){
    $shortopts = "";
    $shortopts .= "u:";  // Required value
    $shortopts .= "p:";  // Required value
    $shortopts .= "h:";  // Required value
    $shortopts .= "db:";  // Required value

    $longopts = array(
        "file:",     // Required value
        "create_table",        // No value
        "help",
        "dry_run",
    );

    $options = getopt($shortopts, $longopts);


    if (array_key_exists('help', $options)) {
        helpInfo();
        exit();    //no further action
    }

    if (array_key_exists('dry_run', $options)) {
        $file = checkFile($options);
        insertRecords($file, null); //no need for db connection
        exit();   //no further action
    }

    //Now we need db connection
    if (!isset($options['u']) || !isset($options['p'])) {
        throw new Exception('Missing db user or password.');
    }

    $dbhost = isset($options['h']) ?: 'localhost';
    $dbname = isset($options['db']) ?: $options['u'];  //default to db user if empty

    $db = new database($options['u'], $options['p'], $dbhost, $dbname);

    if (array_key_exists('create_table', $options)) {
        $db->create_user_table();
        exit();    //no further action
    }

    $file = checkFile($options);
    $users = insertRecords($file, $db);
    $db->insertRecords($users);
}

init();


